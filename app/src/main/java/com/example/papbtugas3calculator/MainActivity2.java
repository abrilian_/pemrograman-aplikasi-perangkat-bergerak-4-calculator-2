package com.example.papbtugas3calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity implements View.OnClickListener {

    TextView angka;
    TextView jenisoperasi;
    Button btback;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        angka = findViewById(R.id.hasil);
        jenisoperasi = findViewById(R.id.operasi);
        btback = findViewById(R.id.btback);
        btback.setOnClickListener(this);

        Intent intent = getIntent();
        String hasil = intent.getStringExtra("Hasil");
        String operasi = intent.getStringExtra("Operasi");

        angka.setText(hasil);
        jenisoperasi.setText(operasi);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btback) {
            finish();
        }
    }
}